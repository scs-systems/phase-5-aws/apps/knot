# knot

knot = KNOwledge Tool.

This package could really be split into three separate packages.
But for this first release I have bundled them together.

1.	knot.py
	Core knot module.
	Collects knowledge from a knowledege catalog - and can obtan knowledge as defined in catalog definition

2.	A default knowledge catalog.
	A default knowledge catalog is shipped with this release. See data/knocat_os.json
	Future release will be enhanced to handle multiple used defined catalogs - 
	including catalog collection over http.

3.	A sample knot interface tool - "knoti", which is a sample CLI
	Various interfaces could be developed to use the core knot module.
	knoti is a sample cli.
	Other interfaces can/will be developed and shipped as separate packages - eg: a web-interface.

## Installation

Package Registry is here:
https://gitlab.com/scs-systems/phase-5-aws/apps/knot/-/packages

For latest version:

```pip install
pip install knot --index-url https://gitlab.com/api/v4/projects/46469785/packages/pypi/simple
```

Note: For debian developers
If you are not installing using pip and/or pipenv - and you instead just clone the knot repository
You may need to ensure you have /usr/bin/python configred for python3
ie:
```Configure python for python3 - as root run:
apt install python-is-python3
```

## Usage

To run the command line interface, "knoti" :

Examples:

```knoti CLI
knoti
knoti -h
knoti memTotalK
knoti memTotalK numcpus procspeedMhz
```

## Contributing

None

## License

Richard Jamieson
