#!/usr/bin/env python
import argparse
import pprint

def main():
   """
   knoti = KNOwlege Tool Command-Line Interface.
   Uses the knot ( KNOwledge Tool ) library.
   """

   parser = argparse.ArgumentParser(description = 'knoti = KNOwledge Tool command-line Interface')
   parser.add_argument('knowledge_items', help='items of knowledge',nargs='*')
   parser.add_argument('-version', '-v', action='version', version='%(prog)s 3.0.0')

   group = parser.add_mutually_exclusive_group()
   group.add_argument('-n', help='tell me variation_name of matching knowledge variation', action='store_true')
   group.add_argument('-t', help='tell me variation_type of matching knowledge variation',action='store_true')
   group.add_argument('-H', help='tell me variation_howto of matching knowledge variation',action='store_true')
   group.add_argument('-c', help='tell me variation knowledge catalog entry of matching knowledge variation',action='store_true')

   args = parser.parse_args()

   if args.n:
       knowledge_request = 'name'
   elif args.t:
       knowledge_request = 'type'
   elif args.H:
       knowledge_request = 'howto'
   elif args.c:
       knowledge_request = 'knowledge_catalog_entry'
   else:
       knowledge_request = 'execute'

   if knot_debug : print("---------------------------------------------------")
   if knot_debug : print("# Starting knoti")
   if knot_debug : print("---------------------------------------------------")
   if knot_debug : print(f"knowledge_items: {args.knowledge_items}")

   if args.knowledge_items:
       for knowledge_item in args.knowledge_items:
           if knot_debug : print(f"knowledge_item : {knowledge_item}")
           knowledge_result = knot(knowledge_item=knowledge_item,
                                   knot_catalog_file=knot_catalog_file,
                                   knot_cache_file=knot_cache_file,
                                   knowledge_request=knowledge_request)
           if knowledge_request == 'knowledge_catalog_entry':
              pp = pprint.PrettyPrinter(width=100, sort_dicts=False)
              pp.pprint(knowledge_result)
           else:
              print(knowledge_result)
   else:
       if knot_debug : print('No arguments, so list all options')
       print(f"{'knowledge_item':20}description                                 ")
       print(f"{'--------------':20}--------------------------------------------")
       (knot_catlog_data,knot_cache_data) = knot(knot_catalog_file=knot_catalog_file,knot_cache_file=knot_cache_file)
       for knowledge_item in knot_catlog_data['catalog_knowledge']:
           knowledge_description = knot_catlog_data['catalog_knowledge'][knowledge_item]['description']
           print(f"{knowledge_item:20}{knowledge_description}")

   if knot_debug : print("---------------------------------------------------")
   if knot_debug : print("# Finished knoti")
   if knot_debug : print("---------------------------------------------------")


if __name__ == "__main__":
    # This for when developing  - or running direct
    from knot import *
    main()
else:
    # THis run run post package knot install  - eg: with pip
    from knot.knot import *

