"""
Core knot (KNOwledge Tool) Module

Environment Variables:
    KNOT_DEBUG          If set to ANY value, output will include verbose debug messages (default == unset )
    KNOT_USE_CACHEFILE  If set to ANYTHING except 'Yes" cache file will be used (default == 'Yes')
    KNOT_CACHE_FILE     PATH to cache file ( default == ~/.knot_cache )
    KNOT_GATALOG_FILES  Unused - future release.
"""

import subprocess
import shlex
import re
import json
import pkg_resources
from os import path, remove, getenv
import pprint
import sys
from io import StringIO

#-----------------------------------------------------------------------
# Module Variables
knot_debug = getenv('KNOT_DEBUG',False)
knot_debug2 = getenv('KNOT_DEBUG','Fred')
knot_use_cachefile = getenv('KNOT_USE_CACHEFILE', 'Yes')

default_knot_catalog_file = pkg_resources.resource_filename('knot', 'data/knocat_os.json')
default_knot_cache_file = path.expanduser('~/.knot_cache')

knot_catalog_file = getenv('KNOT_CATALOG_FILE', default_knot_catalog_file)
knot_cache_file = getenv('KNOT_CACHE_FILE', default_knot_cache_file)
#-----------------------------------------------------------------------

class Knowledge_variation():
    """
    A variation of a knowledge item
    A variation describes one way to obtain a piece of knowledge
    A variation can have "conditions" that determine whether or not the variation is applicable/relevant.
    eg: An example with multiple variation conditions
        {
            "variation_name": "Large People Shirt Size",
            "variation_type": "text",
            "variation_howto": "Large",
            "variation_conditions": [
                                        "'K_rich_height' == 'tall'",
                                        "'K_rich_weight' == 'heavy'"
                                    ]
        }
    """

    def __init__(self, knot_catlog_data, knot_cache_data, variation_name, variation_type, variation_howto, variation_conditions):
        self.knot_catlog_data = knot_catlog_data
        self.knot_cache_data = knot_cache_data
        self.name = variation_name
        self.type = variation_type
        self.howto = variation_howto
        self.conditions = variation_conditions

    def __str__(self):
        details = f"variation name: {self.name}\
                    \n variation_type:\t{self.type}\
                    \n variation_howto:\t{self.howto}\
                    \n variation_conditions:\t{self.conditions}\
                    "
        return details

    def evaluate_condition(self,variation_condition):
        """
        Evaluates a single variation condition - eg:   "'K_rich_height' == 'tall'"
        Returns True is conditions evaluate correctly - or False if not
        If the condition contains a reference to a knowledge_item ( "K_".... ), 
        knot is use to look up that knowledge item before the evaluation
        :return:    The result of the variation_condition = True or False
        """

        if knot_debug : print(f"variation_condition : {variation_condition}")
        # What knowledge do we need to obtain in order to evaluate this condition
        required_knowledge_items = re.findall(r'(?<=K_)\w+', variation_condition)

        if knot_debug : print(f"required_knowledge_items: {required_knowledge_items}")

        if required_knowledge_items :
            for required_knowledge_item in required_knowledge_items:
                result = knot(knot_catlog_data=self.knot_catlog_data,
                         knot_cache_data=self.knot_cache_data,
                         knowledge_item=required_knowledge_item,
                         first_run=False)
                variation_condition = variation_condition.replace('K_' + required_knowledge_item , result)
                if knot_debug : print(f"variation_condition: {variation_condition}")

        if knot_debug : print(f"Evaluate variation_condition : {variation_condition}")
        if not eval(variation_condition):
            if knot_debug : print("Evaluation failed")
            return False
        else:
            if knot_debug : print("Evaluation passed")
            return True


    def evaluate_conditions(self, variation_conditions):
        """
        A knowledge_variation may have multiple conditions - eg:
            "variation_conditions": [
                                        "'K_rich_height' == 'tall'",
                                        "'K_rich_weight' == 'heavy'"
                                    ]
        Each variation_condition wil be tested.
        If ALL conditions are True then this this the right variation to use - function returns True
        :return:    The result of evaluating all of the variation_conditions = True or False
        """

        #if knot_debug : print(f"variation_conditions {variation_conditions}")
        do_all_conditions_match = True
        for variation_condition in variation_conditions:
            if self.evaluate_condition(variation_condition) == False :
                #print("Failed to match")
                do_all_conditions_match = False
                break
        return do_all_conditions_match

    def execute(self):
        """
        One the correct variaton has been selected, it can be executed.
        Mode of execution depends on the variation_type and variation_howto
        eg:
            "variation_type": "text",
            "variation_howto": "Large",
        Currect execution types are:
        *) text         Just return whatever text is the in the variation_howto
        *) oscommand    Run the variation_howto as a OS command
        *) shell        Run the variation_howto as a OS shell command - slight difference to OS command ( try it ) 
        *) pycode_exec  Exec the variation_howto as a python code 
        :return:    The result of the variation_howto execution
        """

        if knot_debug: print(f"Execution in progress")
        if knot_debug: print(f"Execution variation_type: {self.type}")
        if knot_debug: print(f"Execution variation_howto: {self.howto}")

        if self.type == "text":
            result = self.howto
        if self.type == "oscommand":
            command = shlex.split(self.howto)
            process = subprocess.run(command,
                                     stdout=subprocess.PIPE,
                                     universal_newlines=True)
            result = process.stdout
        if self.type == "shell":
            command = self.howto
            process = subprocess.run(command,
                                     stdout=subprocess.PIPE,
                                     universal_newlines=True,
                                     shell=True)
            result = process.stdout
        if self.type == "pycode_exec":
            old_stdout = sys.stdout
            redirected_output = sys.stdout = StringIO()
            try:
                exec(self.howto)
            except:
                raise
            finally: # !
                sys.stdout = old_stdout # !

            result = redirected_output.getvalue()

        return result

def knot(
        knot_catlog_data={},
        knot_cache_data={},
        knowledge_item='knot_list_all',
        first_run=True,
        knot_catalog_file=None,
        knot_cache_file=None,
        knowledge_request='execute'
        ):
    """
    Core knot function
    Next: Add options - knot_option= display json - or give command - or give result as json
    :param knot_catlog_data:    The knowledge catalog
    :param knot_cache_data:     Cached knowledge
    :param knowledge_item:      requested knowledge item - optional
    :param first_run:           True/False  - Can use False for "recursive" runs which may behave differently to first call
    :param knot_catalog_file:   Name of the catalog file - if no cache ( eg: first_run ) need to capture the catalog data
    :param knot_cache_file:     Name of the cache file (eg: so cache can be populated on first_run, or stored after run )
    :param knowledge_request:   type of knowledge_request - optional ( knowledge_catalog_entry, name, type, howto, execute )
     
    Mode-1:
        If called without a request for a specific knowledge_item - knowledge_item will defaut to 'knot_list_all'
        Function will just return the contents of the catalog and the cache
        :return:    (knot_catlog_data,knot_cache_data)

    Mode-2:
        If the request if for a specific knowledge_item, 
        1.  all of the knowledge variations for the piece of knowledge will be assessed (their variation_conditions evaluated) 
        2.  THe first applcable knowledge_variation that is found will be "executed"/evaluated
        3.  The results of the evaluation willbe cached by default
        :return:    The result of the knowledege evaluation
    """

    if knot_debug : print("---------------------------------------------------")
    if knot_debug : print(f"Calling knot. knowledge_item: {knowledge_item}")
    if knot_debug : print(f"Calling knot. first_run: {first_run}")
    if knot_debug : print(f"Calling knot. knot_catalog_file: {knot_catalog_file}")
    if knot_debug : print(f"Calling knot. knot_cache_file: {knot_cache_file}")
    if knot_debug : print(f"Calling knot. knowledge_request: {knowledge_request}")

    ##################################################################ddP
    # First-Run CODE
    # So need to initialise knot_catlog_data and knot_cache_data

    if knot_debug : print(f"First Run:  { first_run}")

    if first_run == True:

       # Initialise the Catalog
       try:
          with open(knot_catalog_file) as f_catalog:
             knot_catlog_data = json.load(f_catalog)
       except FileNotFoundError as err:
           print(f"Error: Catalog File not found {err}")
           exit(1)
       except Exception as err:
           print(f"Error: Cache File error, when reading the cache {err}")
           exit(1)

       # Initialise the Cache
       if knot_debug : print(f"knot_use_cachefile:  { knot_use_cachefile}")
       if knot_use_cachefile == 'Yes':
          if path.isfile(knot_cache_file):
             try:
                with open(knot_cache_file) as f_cache:
                   knot_cache_data = json.load(f_cache)
             except Exception as err:
                print(f"Error: Cache File error {err}")
                exit(1)
          else:
             knot_cache_data = {}
       else:
           knot_cache_data = {}

    
    # First Run initialisation code - Ends
    ##################################################################ddP


    if knowledge_item == "knot_list_all" :
        result = (knot_catlog_data,knot_cache_data)

    elif knowledge_item not in knot_catlog_data['catalog_knowledge']:
        if knot_debug: print(f"Requested knowledge item not in catalog :  {knowledge_item}")
        result = None

    elif knowledge_item in knot_cache_data and knowledge_request == 'execute':
           if knot_debug : print(f"Found {knowledge_item} in cache")
           result = knot_cache_data[knowledge_item]

    else:
       variation_count = 0
       for variation_item in knot_catlog_data['catalog_knowledge'][knowledge_item]['variations']:
           if knot_debug : print("----------------------")
           variation = Knowledge_variation(
               knot_catlog_data=knot_catlog_data,
               knot_cache_data=knot_cache_data,
               variation_name=variation_item['variation_name'],
               variation_type=variation_item['variation_type'],
               variation_howto=variation_item['variation_howto'],
               variation_conditions=variation_item['variation_conditions'],
           )
           variation_count += 1
           if knot_debug: print(f"variation_count: {variation_count}")
           if knot_debug: print(variation)
           # Check the conditions
           conditions_match = variation.evaluate_conditions(variation.conditions)
           if knot_debug : print(f"conditions_match: {conditions_match}")
           if conditions_match:
               if knowledge_request == 'name':
                   result = variation.name
               elif knowledge_request == 'type':
                   result = variation.type
               elif knowledge_request == 'howto':
                   result = variation.howto
               elif knowledge_request == 'knowledge_catalog_entry':
                   result = variation_item
               else:
                   # knowledge_request == 'execute'
                   # check if knowledge is already in the cache
                   result = variation.execute().strip()
                   if knot_debug : print(f"result: {result}")
                   if knot_debug : print('--------------------------')
                   # put result in the cache
                   knot_cache_data[knowledge_item] = result
               break

    if first_run == True:
       if knot_debug : print('First Run Competed - now for the cache work')
       if knot_debug : print(f"knot_use_cachefile:  { knot_use_cachefile}")
       if knot_use_cachefile == 'Yes':
          if knot_debug : print(f"First Run - write to knot_cache_file {knot_cache_file}")
          # And write out the cache
          try:
             with open(knot_cache_file, 'w') as f_cache:
                 json.dump(knot_cache_data, f_cache)
          except Exception as err:
             print(f"Error: Cache File error, when writing the cache {err}")
             exit(1)
       else:
          if knot_debug : print(f"First Run - NO write to knot_cache_file {knot_cache_file}")

    if knot_debug : print(f"Returning result: {result}")
    if knot_debug : print("---------------------------------------------------")

    return result
